# FiberFoam #

OpenFOAM solver to simulate diluted fiber suspensions in a Bingham-plastic like fluid.

## Citation ##
If you are using this code, please cite:

Heiko Herrmann and Aarne Lees. *On the influence of the rheological boundary conditions on the fibre orientations in the production of steel fibre reinforced concrete elements*, Proceedings of the Estonian Academy of Sciences, 65(4), 2016 (accepted)

## Disclaimer ##
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.