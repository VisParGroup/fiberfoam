/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------

\*---------------------------------------------------------------------------*/

#include "fiberModel.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace singlePhase
{

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

defineTypeNameAndDebug(fiberModel, 0);
defineRunTimeSelectionTable(fiberModel, fiberModel);

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

fiberModel::fiberModel
(
    const volVectorField& U,
    const surfaceScalarField& phi,
    const word& fiberModelName
)
:
    IOdictionary
    (
        IOobject
        (
            "fiberProperties",
            U.time().constant(),
            U.db(),
            IOobject::MUST_READ_IF_MODIFIED,
            IOobject::NO_WRITE
        )
    ),

    runTime_(U.time()),
    mesh_(U.mesh()),

    U_(U),
    phi_(phi),

    A2_
    (
        IOobject
        (
            "A2",
            runTime_.timeName(),
            mesh_,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh_
    ),

    fiberSuspension_(lookup("fiberSuspension")),

    closureA4Dict_(subOrEmptyDict("fiberClosure")),
    fiberInteractionDict_(subOrEmptyDict("fiberInteraction")),

    closureA4Ptr_(fiberClosure::New("A4", closureA4Dict_, A2_)),
    fiberInteractionPtr_(fiberInteraction::New("CI", fiberInteractionDict_))
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

bool fiberModel::read()
{
    bool ok = IOdictionary::readData
    (
        IOdictionary::readStream
        (
            IOdictionary::type()
        )
    );
    IOdictionary::close();

    if (ok)
    {
        lookup("fiberSuspension") >> fiberSuspension_;

        if (const dictionary* dictPtr = subDictPtr("fiberClosure"))
        {
            closureA4Dict_ <<= *dictPtr;
        }

        if (const dictionary* dictPtr = subDictPtr("fiberInteraction"))
        {
            fiberInteractionDict_ <<= *dictPtr;
        }

        return true;
    }
    else
    {
        return false;
    }
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace singlePhase
} // End namespace Foam

// ************************************************************************* //
