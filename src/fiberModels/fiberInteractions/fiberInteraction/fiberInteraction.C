/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------

\*---------------------------------------------------------------------------*/

#include "fiberInteraction.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(fiberInteraction, 0);
    defineRunTimeSelectionTable(fiberInteraction, dictionary);
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::fiberInteraction::fiberInteraction
(
    const word& name,
    const dictionary& fiberInteractionDict
)
:
    name_(name),
    fiberInteractionDict_(fiberInteractionDict)
{}


// ************************************************************************* //
